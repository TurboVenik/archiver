package ru.ncedu.vyunn.zipper;

import ru.ncedu.vyunn.zipper.utils.CommandLineParser;

import java.io.IOException;

public class Application {

    public static void main(String[] args) {
        CommandLineParser parser = new CommandLineParser();
        try {
            parser.parse(args);
        } catch (IOException e) {
            System.out.println(String.format("Error: %s",e.getLocalizedMessage()));
        }
    }
}
