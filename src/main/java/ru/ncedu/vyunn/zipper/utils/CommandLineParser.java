package ru.ncedu.vyunn.zipper.utils;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Class that parse program input using <code>JCommander</code>.
 * It defines what actions the utility should perform using an instance of the <code>Zipper<code/> class.
 * @see Zipper
 * @see JCommander
 */
public class CommandLineParser {

    /**
     * Determines what actions the utility should perform and executes them.
     * @param args parameters of the command line.
     * @throws IOException exceptions from <code>Zipper</code>.
     */
    public void parse(String[] args) throws IOException {
        //Parse the command line.
        CommandLineEntity lineEntity = new CommandLineEntity();
        JCommander commander = JCommander.newBuilder()
                .addObject(lineEntity)
                .build();
        try {
            commander.parse(args);
        } catch (ParameterException e) {
            System.out.println(String.format("Error: %s", e.getLocalizedMessage()));
            commander.usage();
            return;
        }

        if (lineEntity.isHelp()) {
            commander.usage();
            return;
        }
        if (lineEntity.isZip()) {
            zip(lineEntity.getName(), lineEntity.getSource(), lineEntity.getComment());
        } else if (lineEntity.isUnzip()){
            unZip(lineEntity.getName(), lineEntity.getPath());
        } else if (lineEntity.isAdd()) {
            addToZip(lineEntity.getName(), lineEntity.getSource(), lineEntity.getComment());
        }else if (lineEntity.isChangeComment()) {
            changeComment(lineEntity.getName(), lineEntity.getComment());
        } else if (lineEntity.isReadComment()) {
            readComment(lineEntity.getName());
        } else {
            System.out.println("Error: Wrong parameters.");
            commander.usage();
        }
    }

    /**
     * Create archive.
     * @param archiveName name of the archive.
     * @param source list of files.
     * @param comment comment of the archive.
     * @throws IOException exceptions from <code>Zipper</code>.
     */
    private void zip(String archiveName, List<String> source, String comment) throws IOException {
        if (source == null) {
            System.out.println("Error: Expected parameter -s");
            return;
        }
        for (String fileName : source) {
            if (checkFile(fileName)) {
                return;
            }
        }
        if (archiveName == null || archiveName.equals("")) {
            archiveName = "default_name";
        }
        new Zipper()
                .zip(archiveName, source, comment);
    }

    /**
     * Unzip the archive.
     * @param archiveName name of the archive.
     * @param path the folder into which the unzipping will be performed. To the same directory if it is null or "".
     * @throws IOException exceptions from <code>Zipper</code>.
     */
    private void unZip(String archiveName, String path) throws IOException {
        if (path != null) {
            File file = new File(path);
            if (!file.exists()) {
                if (!file.mkdirs()) {
                    System.out.println(String.format("Error: can not create directiory %s", path));
                }
            }
        }
        if (path == null) {
            path = "";
        } else if (!path.endsWith("/")) {
            path += "/";
        }
        if (archiveName == null || archiveName.equals("")) {
            System.out.println("Error: Expected parameter -n");
            return;
        }
        new Zipper().unZip(archiveName, path);
    }

    /**
     * Adds files to archive.
     * @param archiveName archive archiveName.
     * @param source list of new files.
     * @param comment new comment if null old comment will be used.
     * @throws IOException exceptions from <code>Zipper</code>.
     */
    private void addToZip(String archiveName, List<String> source, String comment) throws IOException {
        if (source == null) {
            System.out.println("Error: Expected parameter -s.");
            return;
        }
        if (archiveName == null) {
            System.out.println("Error: Expected parameter -n.");
            return;
        }
        if (checkFile(archiveName)) {
            return;
        }

        for (String fileName : source) {
            if (checkFile(fileName)) {
                return;
            }
        }

        new Zipper()
                .addToZip(archiveName, source, comment);
    }

    /**
     * Reads comment of the archive.
     * @param archiveName archiveName of the archive.
     * @throws IOException exceptions from <code>Zipper</code>.
     */
    private void readComment(String archiveName) throws IOException {
        if (archiveName == null) {
            System.out.println("Error: Expected parameter -n");
            return;
        }
        if (checkFile(archiveName)) {
            return;
        }
        String comment = new Zipper().readComment(archiveName);
        System.out.println(String.format("Archive:  %s", archiveName));
        System.out.println(comment == null ? "" : comment);
    }

    /**
     * Changes comment of the archive.
     * @param archiveName archiveName of the archive.
     * @param comment new comment.
     * @throws IOException exceptions from <code>Zipper</code>.
     */
    private void changeComment(String archiveName, String comment) throws IOException {
        if (archiveName == null || archiveName.equals("")) {
            System.out.println("Error: Expected parameter -n");
            return;
        }
        if (checkFile(archiveName)) {
            return;
        }
        new Zipper().changeComment(archiveName, comment == null ? "" : comment);
    }


    /**
     * Check file if it exists or can be readed
     * @param fileName fileName of the file.
     * @return true if file can not be read.
     */
    private boolean checkFile(String fileName) {
        File file = new File(fileName);
        if (!file.exists()) {
            System.out.println(String.format("Error: There is no file '%s'.", fileName));
            return true;
        }
        if (!file.canRead()) {
            System.out.println(String.format("Error: File '%s' can not be readed.", fileName));
            return true;
        }
        return false;
    }
}
