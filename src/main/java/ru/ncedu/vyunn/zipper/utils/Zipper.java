package ru.ncedu.vyunn.zipper.utils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.List;
import java.util.Random;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 * A class that provides the ability to work with archives.
 * @see ZipOutputStream
 * @see ZipFile
 * @see ZipEntry
 */
public class Zipper {

    /**
     * Creates archive.
     * @param archiveName the name of the archive to be created.
     * @param source list of files that will be included in the archive.
     * @param comment comment of the archive.
     * @throws IOException exceptions that occur during the creation of an archive and reading files.
     */
    public void zip(String archiveName, List<String> source, String comment) throws IOException {
        try (ZipOutputStream out = new ZipOutputStream(new FileOutputStream(archiveName))) {
            for (String fileName : source) {
                File file = new File(fileName);
                addFileToZip(file, out);
            }
            out.setComment(comment);
        }
    }

    /**
     * Unzip archive.
     * @param archiveName the name of the target archive.
     * @param path folder in which files from the archive will be added.
     * @throws IOException exceptions that occur during reading of the archive or creating files and directories.
     */
    public void unZip(String archiveName, String path) throws IOException {
        ZipFile zip = new ZipFile(archiveName);
        Enumeration<? extends ZipEntry> entries = zip.entries();
        while (entries.hasMoreElements()) {
            ZipEntry entry = entries.nextElement();
            if (entry.isDirectory()) {
                new File(path + entry.getName()).mkdirs();
            } else {
                String s = entry.getName().substring(0, entry.getName().lastIndexOf("/") + 1);
                new File(path + s).mkdirs();
                try (InputStream inputStream = zip.getInputStream(entry);
                     OutputStream outputStream
                             = new BufferedOutputStream(new FileOutputStream(new File(path + entry.getName())))) {
                    write(inputStream, outputStream);
                }
            }
        }
    }

    /**
     * Adds files to existing archive.
     * Creates new archive which will include file from the 'archiveName' archive and new files.
     * Old archive will be deleted while new archive will have the same name.
     * @param archiveName the name of the archive to which the files will be add.
     * @param source list of new files that will be included in the archive.
     * @param comment comment of new archive. If it is null then old comment will be save.
     * @throws IOException exceptions that occur during reading or creating of the archives and creating files or directories
     */
    public void addToZip(String archiveName, List<String> source, String comment) throws IOException {
        // Creating new archive with random name.
        String tmpName = "_" + new Random().nextInt() + archiveName;
        try (ZipOutputStream out = new ZipOutputStream(new FileOutputStream(tmpName))) {
            // Add all files to new archive from target archive.
            ZipFile zip = new ZipFile(archiveName);
            Enumeration<? extends ZipEntry> entries = zip.entries();
            while (entries.hasMoreElements()) {
                ZipEntry entry = entries.nextElement();
                if (entry.isDirectory()) {
                    out.putNextEntry(new ZipEntry(entry.getName()));
                } else {
                    out.putNextEntry(new ZipEntry(entry.getName()));
                    try (InputStream inputStream = zip.getInputStream(entry)) {
                        write(inputStream, out);
                    }
                }
            }
            // Add all new files to the new archive.
            for (String fileName : source) {
                File file = new File(fileName);
                addFileToZip(file, out);
            }
            out.setComment(comment == null ? zip.getComment() : comment);

        }
        // Delete target archive.
        Files.delete(Paths.get(archiveName));
        // Rename new archive.
        File newFile = new File(archiveName);
        File tmpFile = new File(tmpName);
        tmpFile.renameTo(newFile);
    }

    /**
     * Reads comment of the archive.
     * @param archiveName name of the archive.
     * @return comment of the archive.
     * @throws IOException exceptions that occur during reading information about archive.
     */
    public String readComment(String archiveName) throws IOException {
        ZipFile zipFile = new ZipFile(archiveName);
        return zipFile.getComment();
    }

    /**
     * Changes comment of the archive.
     * @param archiveName name of the archive.
     * @param comment new comment.
     * @throws IOException exceptions that occur during reading or writing information to the archive.
     */
    public void changeComment(String archiveName, String comment) throws IOException {
        ZipOutputStream out = new ZipOutputStream(new FileOutputStream(archiveName));
        out.setComment(comment);
        out.close();
    }

    /**
     * Adds the contents of the file to the archive, if a directory is transferred instead of the file,
     * its contents are recursively written to the archive.
     * @param file name of file or directory.
     * @param out output stream of the target archive.
     * @throws IOException exceptions that occur during writing information to the archive.
     */
    private void addFileToZip(File file, ZipOutputStream out) throws IOException {
        if (file.isDirectory()) {
            if (file.listFiles().length == 0) {
                out.putNextEntry(new ZipEntry(file.getPath() + File.separator));
                return;
            }
            for (File f : file.listFiles()) {
                if (f.isDirectory()) {
                    addFileToZip(f, out);
                } else {
                    out.putNextEntry(new ZipEntry(f.getPath()));
                    try (FileInputStream inputStream = new FileInputStream(f)) {
                        write(inputStream, out);
                    }
                }
            }
        } else {
            out.putNextEntry(new ZipEntry(file.getPath()));
            try (FileInputStream inputStream = new FileInputStream(file)) {
                write(inputStream, out);
            }
        }
    }

    /**
     * Transmits information from InputStream to OutputStream.
     * @param in InputStream.
     * @param out OutputStream.
     * @throws IOException exceptions that occur during writing or reading from streams.
     */
    private void write(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[in.available()];
        int len;
        while ((len = in.read(buffer)) >= 0)
            out.write(buffer, 0, len);
    }
}
