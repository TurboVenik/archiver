package ru.ncedu.vyunn.zipper.utils;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

import java.util.List;

/**
 * Class that describes the rules for working with the program through the console.
 * @see JCommander
 * @see Parameter
 */
public class CommandLineEntity {

    @Parameter(names = { "-z", "--zip" },
            description = "creating new archive.")
    private boolean zip;

    @Parameter(names = { "-u", "--unzip" },
            description = "unzip existing archive.")
    private boolean unzip;

    @Parameter(names = { "-a", "--add" },
            description = "add files to existing archive.")
    private boolean add;

    @Parameter(names = { "-cc", "--changeсomment" },
            description = "change comment of the existiong archive.")
    private boolean changeComment;

    @Parameter(names = { "-rc", "--readсomment" },
            description = "shows comment of the archive")
    private boolean readComment;

    @Parameter(names = { "-s", "--source" },
            description = "list of files")
    private List<String> source;

    @Parameter(names = { "-n", "--name"},
            description = "name of archive")
    private String name = null;

    @Parameter(names = { "-c", "--comment" },
            description = "comment of archive")
    private String comment = null;

    @Parameter(names = { "-p", "--path" },
            description = "folder")
    private String path = null;

    @Parameter(names = {"-h", "--help"}, help = true,
            description = "help message")
    private boolean help;

    public boolean isZip() {
        return zip;
    }

    public boolean isUnzip() {
        return unzip;
    }

    public boolean isAdd() {
        return add;
    }

    public List<String> getSource() {
        return source;
    }

    public boolean isChangeComment() {
        return changeComment;
    }

    public String getComment() {
        return comment;
    }

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }

    public boolean isReadComment() {
        return readComment;
    }

    public boolean isHelp() {
        return help;
    }
}
